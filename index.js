/**
 * @format
 */

import { AppRegistry } from 'react-native';
import RootNode from './src/Routes/RootNode';
import {name as appName} from './app.json'

AppRegistry.registerComponent(appName, () => RootNode);
