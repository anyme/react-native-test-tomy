import React, {Component} from 'react';
import { Provider } from 'react-redux';
import AppViewContainer from '../Components/User/ViewContainer';
import { configureStore } from '../Store/store';
import { Provider as ProviderPaper } from "react-native-paper";

const initialState = window.initialReduxState;
const store = configureStore(initialState);

export default class RootNode extends Component {
    render() {
        return (
            <Provider store={store}>
                <ProviderPaper>
                    <AppViewContainer />
                </ProviderPaper>
            </Provider>
        );
    }
}

