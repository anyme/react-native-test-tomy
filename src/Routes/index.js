import React, { Component } from 'react';
import { Router, Scene, Stack } from 'react-native-router-flux';
import { connect } from 'react-redux';
import UserList from '../Components/User/ViewContainer';

class AllRoutes extends Component {
	render() {
		return (
			<Router>
				<Scene key='root' hideNavBar panHandlers={null}>
                    {UserList}
				</Scene>
			</Router>
		)
	}
}

export default connect(AllRoutes)

