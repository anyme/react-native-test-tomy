export const RETRIEVE_USER_LIST = 'userlist/RETRIEVE_USER_LIST';
export const RETRIEVE_USER_LIST_ERROR = 'userlist/RETRIEVE_USER_LIST_ERROR';
export const RETRIEVE_USER_LIST_SUCCESS = 'userlist/RETRIEVE_USER_LIST_SUCCESS';

export const HIDE_USER = 'userlist/HIDE_USER';
export const HIDE_USER_SUCCESS = 'userlist/HIDE_USER_SUCCESS';