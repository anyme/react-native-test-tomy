import { action } from 'typesafe-actions';
import  * as UserListActionTypes  from './Types';

////////////// Receive data

export const retrieveUserList = () => action(UserListActionTypes.RETRIEVE_USER_LIST);
export const retrieveUserListSuccess = (userList) =>  action(UserListActionTypes.RETRIEVE_USER_LIST_SUCCESS, userList);
export const retrieveUserListError = () => action(UserListActionTypes.RETRIEVE_USER_LIST_ERROR, []);

export const hideUser = (userId) => action(UserListActionTypes.HIDE_USER, userId);
export const hideUserSuccess = (hiddenUserIds) => action(UserListActionTypes.HIDE_USER_SUCCESS, hiddenUserIds);
