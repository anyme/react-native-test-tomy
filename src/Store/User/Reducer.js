import { ActionConst } from 'react-native-router-flux';
import { Reducer } from 'redux';
import  * as UserListActionTypes  from './Types';

export const initialState = [];

const UserListReducer = (state = initialState, action) => {
  switch (action.type) {
    case UserListActionTypes.RETRIEVE_USER_LIST_SUCCESS:
        return action.payload;
        
    case UserListActionTypes.HIDE_USER_SUCCESS:
        let hiddenUser = action.payload;
        let userList = state;
        let res =  userList.filter((item) => !hiddenUser[item.id])
        return res;

    default:
        return state;
  }
}

export {UserListReducer};