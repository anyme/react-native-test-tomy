import * as effects from 'redux-saga/effects';
// import { AsyncStorage } from "@react-native-community/async-storage";
import { AsyncStorage } from 'react-native';

import axios from 'axios';
import { retrieveUserListSuccess, hideUserSuccess } from './Actions';
import * as UserListActionTypes  from './Types';
import {HOST_URL} from '../../config';

function* handleRetireveUserList() {
    let response = yield axios.get(HOST_URL + 'users');

    if (response.status < 400) {
        yield handleRetrieveUserListSuccess(response.data);
    } else {
        yield effects.put(retieveUserDetails(results.message));
    }
}

function* handleRetrieveUserListSuccess(allUsers) {
    // yield AsyncStorage.setItem('hidden_users', "[]");
    let hiddenUsers = yield AsyncStorage.getItem('hidden_users');
    if (!hiddenUsers) {
        hiddenUsers = {}
    } else {
        hiddenUsers = JSON.parse(hiddenUsers);
    }
    
    yield effects.put(retrieveUserListSuccess(allUsers));
    yield effects.put(hideUserSuccess(hiddenUsers));
}

function* handleHideUser(action) {
    let userId = action.payload;
    let hiddenUsers = yield AsyncStorage.getItem('hidden_users', (error, result) => (error ? null: result));
    if (!hiddenUsers) {
        hiddenUsers = {}
    } else {
        hiddenUsers = JSON.parse(hiddenUsers);
    }

    hiddenUsers[userId] = true
    yield AsyncStorage.setItem('hidden_users', JSON.stringify(hiddenUsers));
    yield effects.put(hideUserSuccess(hiddenUsers));
}

function* watchUserList() {
    yield effects.takeEvery(UserListActionTypes.RETRIEVE_USER_LIST, handleRetireveUserList);
    yield effects.takeEvery(UserListActionTypes.HIDE_USER, handleHideUser);
}

export function* UserListSaga() {
    yield effects.all([
        effects.fork(watchUserList),
    ]);
}
