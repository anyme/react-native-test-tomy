import { combineReducers } from 'redux';
import * as effects from 'redux-saga/effects';
import { UserListSaga } from './User/Saga';
import { UserListReducer } from './User/Reducer';

export const RootReducer = combineReducers({
    userList: UserListReducer
});

export function* RootSaga() {
    yield effects.all([
        effects.fork(UserListSaga),
    ]);
}
