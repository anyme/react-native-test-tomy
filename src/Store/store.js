import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
// import rootReducer from '../Store';
import { RootReducer, RootSaga } from '../Store';

let store= null;

export function configureStore (initialState) {
    const sagaMiddleware = createSagaMiddleware();
    store = createStore(
        RootReducer,
        initialState,
        applyMiddleware(sagaMiddleware)
    );
    sagaMiddleware.run(RootSaga);

    return store;
}

export { store };