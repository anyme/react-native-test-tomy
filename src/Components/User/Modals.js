import { List } from 'native-base';
import * as React from 'react';
import { Modal, Portal, Text, Button, Caption, Card , Title, Paragraph} from 'react-native-paper';

export default class UserDetailModal extends React.Component {
    containerStyle;

    constructor(props) {
        super();
        this.containerStyle = {backgroundColor: 'white', padding: 20};
        this.state = {
            visible: false,
            userDetails: {
                id: null,
                name: "",
                username: "",
                email: "",
                address: {
                    street: "",
                    suite: "",
                    city: "",
                    zipcode: "",
                    geo: {
                        lat: null,
                        lng: null
                    }
                },
                phone: 0,
                website: "",
                company: {
                    name: "",
                    catchPhrase:"",
                    bs: ""
                }
            }
        }
    };

    showModal = () => {
        if (this.state.userDetails.id !== null) {
            this.setState({
                visible: true
            });
        }
    }

    hideModal = () => {
        this.setState({
            visible: false
        })
    }

    setUserDetails = (userDetails) => {
        this.setState({
            userDetails: userDetails
        })
    }

    onHideUserClicked = () => {
        this.props.hideUser(this.state.userDetails);
        this.hideModal();
    }

    render() {
        return (
            <Portal>
                <Modal visible={this.state.visible} onDismiss={this.hideModal} contentContainerStyle={this.containerStyle}>
                    <Card>
                        <Card.Content>
                            <Title>{this.state.userDetails.name}</Title>
                            <Paragraph><Text style={{fontSize: 10, color: 'rgba(0, 0, 0, 0.4'}}>username: </Text>{this.state.userDetails.username}</Paragraph>
                            <Paragraph><Text style={{fontSize: 10, color: 'rgba(0, 0, 0, 0.4'}}>email: </Text> {this.state.userDetails.email}</Paragraph>
                            <Paragraph><Text style={{fontSize: 10, color: 'rgba(0, 0, 0, 0.4'}}>phone: </Text>{this.state.userDetails.phone}</Paragraph>
                            <Paragraph><Text style={{fontSize: 10, color: 'rgba(0, 0, 0, 0.4'}}>compan </Text>: {this.state.userDetails.company.name}</Paragraph>
                            <Paragraph><Text style={{fontSize: 10, color: 'rgba(0, 0, 0, 0.4'}}>website: </Text>{this.state.userDetails.website}</Paragraph>
                            <Paragraph><Text style={{fontSize: 10, color: 'rgba(0, 0, 0, 0.4'}}>address: </Text>{this.state.userDetails.address.street}, {this.state.userDetails.address.city}</Paragraph>
                        </Card.Content>
                    </Card>
                    <Card.Actions>
                        <Button onPress={this.hideModal}>OK</Button>
                        <Button onPress={this.onHideUserClicked}>Hide User</Button>
                    </Card.Actions>
                </Modal>
            </Portal>
           
        );
  }
}