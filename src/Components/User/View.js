import React from 'react';
import {
    ScrollView,
} from 'react-native';
import { List, Text, Searchbar, Button } from 'react-native-paper';

import UserDetailModal from './Modals';

function filterUser(item, searchQuery) {
    if (typeof(item) === 'Object') {
        // Launch recursion for object children
        if (deepFilterUserR(item)) {
            return true;
        }
    } else if (typeof(item) === 'string') {
        if (item.includes(searchQuery)) {
            return true;
        }
    } else if (typeof(item) !== 'boolean') {
        if (JSON.stringify(item).includes(searchQuery)) {
            return true;
        }
    }
    return false;
}

function deepFilterUserR(attributes, searchQuery) {
    let attributeValues = Object.values(attributes);
    for (let i = 0; i < attributeValues.length; i += 1) {
        if (filterUser(attributeValues[i], searchQuery)) {
            return true;
        }
    }

    return false;
}

export default class FirstScreenView extends React.Component {
    userDetailsModalRef;

    constructor(props) {
        super();
        this.userDetailsModalRef = React.createRef();
        this.state = {
            order: null,
            searchQuery: "",
            userList2Display: []
        }
    };
    
    static getDerivedStateFromProps(props, state) {
        let userList = props.userList;
        if (state.searchQuery !== "") {
            state.userList2Display = userList.filter((item) => deepFilterUserR(item, state.searchQuery));
           
        } else {
            state.userList2Display = userList;
        }
        return state;
    }

    componentDidMount() {
        this.props.retrieveUserList();
    }

    handleUserDetailsClick = (user) => {
        this.setUserDetails(user);
        this.showUserDetailsModal()
    }

    onHideUserClicked = (user) => {
        this.props.hideUser(user.id);
        this.hideUserDetailsModal();
    }

    drawUserList = () => {
        const { userList2Display } = this.state;
        if (userList2Display && userList2Display.length > 0) {
            return userList2Display.map((item, index) => {
                return (
                    <List.Item
                        key={'key-'+index}
                        style={{border: "1px solid rgba(0, 0, 0, 1"}}
                        title={item.name}
                        description={item.company.name}
                        onPress={this.handleUserDetailsClick.bind(this, item)}
                        left={props => <Button icon="delete" {...props} onPress={this.onHideUserClicked.bind(this, item)}/>}
                    />);
            })
        } else {
            return <Text>No users found.</Text>
        }
    }

    setUserDetails(userDetails) {
        if (this.userDetailsModalRef && this.userDetailsModalRef.current) {
            this.userDetailsModalRef.current.setUserDetails(userDetails);
        }
    }

    showUserDetailsModal() {
        if (this.userDetailsModalRef && this.userDetailsModalRef.current) {
            this.userDetailsModalRef.current.showModal();
        }
    }

    hideUserDetailsModal() {
        if (this.userDetailsModalRef && this.userDetailsModalRef.current) {
            this.userDetailsModalRef.current.hideModal();
        }
    }

    onChangeSearch = (query) => {
        let filteredUsers = deepFilterUserR(this.state.userList2Display, query);
        this.setState({
            searchQuery: query,
            userList2Display: filteredUsers
        });
    }
    sortUsersByName = () => {
        let order = !this.state.order;
        let users = this.state.userList2Display;
        let desc = -1;
        if (order) {
            desc = 1
        }
        users.sort((a, b) => {
            if (a.name < b.name) {
                return desc;
            }
            if (a.name > b.name) {
                return -desc;
            }
            return 0;
        });

        this.setState({
            order: order,
            userList2Display: users
        });
    }

    drawSearchInput = () => {
        return (
            <Searchbar
                placeholder="Search"
                onChangeText={this.onChangeSearch}
                value={this.state.searchQuery}
            />
        );
    }

    drawSortingButton = () => {
        return (
            <Button
                icon={this.state.order? "sort-ascending": "sort-descending"}
                mode="contained"
                onPress={this.sortUsersByName}
            />
        )
    }
    
    render() {
        return (
            <ScrollView style={{ position: 'relative', width: '100%', height: '100%' }}>
                {this.drawSearchInput()}
                {this.drawSortingButton()}
                <UserDetailModal ref={this.userDetailsModalRef} hideUser={this.onHideUserClicked}/>
                {this.drawUserList()}
            </ScrollView>
        );
    }
}
