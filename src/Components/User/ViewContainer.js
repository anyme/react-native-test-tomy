import { connect } from 'react-redux';
import * as UserListActionTypes from '../../Store/User/Types';
import { hideUser, retrieveUserList } from '../../Store/User/Actions';
import View from './View';

const mapStateToProps = state => ({
  userList: state.userList || {}
});

const mapDispatchToProps = dispatch => ({
    retrieveUserList: () => dispatch(retrieveUserList()),
    hideUser: (userId) => dispatch(hideUser(userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(View);